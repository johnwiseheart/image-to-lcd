import * as React from "react";
import { AppStyles } from "./style";
import { DEFAULT_WRAPPER_TEXT, IAppState } from "./types";
import { convertToTriChrome, getDownloadFile } from "./utils";

class App extends React.Component<{}, IAppState> {
  public state: IAppState = {
    filename: "",
    hasRedColor: false,
    height: "300",
    imageDataData: [],
    nameText: "image",
    useProgmem: true,
    valuesPerLine: 8,
    width: "400",
    wrapperText: DEFAULT_WRAPPER_TEXT
  };

  private canvasInput: HTMLCanvasElement;
  private canvasOutput: HTMLCanvasElement;
  private fileInput: HTMLInputElement;
  private refHandler = {
    canvasInput: (canvas: HTMLCanvasElement) => (this.canvasInput = canvas),
    canvasOutput: (canvas: HTMLCanvasElement) => (this.canvasOutput = canvas),
    fileInput: (input: HTMLInputElement) => (this.fileInput = input)
  };

  public render() {
    return (
      <div className={AppStyles.containerStyle}>
        <div className={AppStyles.appStyle}>
          <header className={AppStyles.appHeaderStyle}>
            <h1>Image to LCD</h1>
            <p>
              Use this tool to convert an image to a C file to be used on an LCD or e-paper screen.<br />
              If your screen supports 3 colors, use the checkbox to select the red color.
            </p>
          </header>

          <main className={AppStyles.mainStyle}>
            {this.renderOptions()}
            {this.renderCanvases()}
            {this.renderDownloadButton()}
          </main>
        </div>
      </div>
    );
  }

  private renderFileInput = () => {
    const { filename } = this.state;
    return (
      <div className={AppStyles.optionsItem}>
        <label className={AppStyles.labelStyle} htmlFor="file">
          Select file
        </label>
        <input
          type="file"
          id="file"
          className={AppStyles.fileInputStyle}
          onChange={this.handleFileChange}
          ref={this.refHandler.fileInput}
        />
        <button className={AppStyles.buttonInputStyle} onClick={this.onFileClick}>
          {filename || "Select image"}
        </button>
      </div>
    );
  };

  private onFileClick = () => this.fileInput.click();

  private renderDownloadButton = () => {
    return (
      <div className={AppStyles.downloadStyle} onClick={this.handleDownload}>
        Download
      </div>
    );
  };

  private renderCheckboxes = () => {
    const { useProgmem, hasRedColor } = this.state;
    return (
      <div>
        <div className={AppStyles.checkboxWrapperStyle}>
          <input
            id="useProgmem"
            className={AppStyles.checkboxStyle}
            type="checkbox"
            checked={useProgmem}
            onChange={this.handleUseProgmemChange}
          />
          <label className={AppStyles.labelStyle} htmlFor="useProgmem">
            Use PROGMEM
          </label>
        </div>

        <div className={AppStyles.checkboxWrapperStyle}>
          <input
            id="hasRedColor"
            className={AppStyles.checkboxStyle}
            type="checkbox"
            checked={hasRedColor}
            onChange={this.handleHasRedColorChange}
          />
          <label className={AppStyles.labelStyle} htmlFor="hasRedColor">
            Screen Has red color
          </label>
        </div>
      </div>
    );
  };

  private renderOptions = () => {
    const { wrapperText, nameText } = this.state;
    return (
      <div className={AppStyles.optionsStyle}>
        <div className={AppStyles.headerStyle}>Options</div>
        <div className={AppStyles.optionsInnerStyle}>
          <div className={AppStyles.optionsItem}>
            {this.renderFileInput()}
            <label className={AppStyles.labelStyle} htmlFor="name">
              Variable name
            </label>
            <input type="text" id="name" value={nameText} onChange={this.handleNameChange} />
            {this.renderCheckboxes()}
          </div>
          <div className={AppStyles.optionsItem}>
            <label className={AppStyles.labelStyle} htmlFor="wrapper">
              Wrapping code around image
            </label>
            <textarea
              id="wrapper"
              className={AppStyles.textareaStyle}
              value={wrapperText}
              onChange={this.handleWrapperTextChange}
            />
          </div>
        </div>
      </div>
    );
  };

  private renderCanvases = () => {
    const { width, height } = this.state;
    return (
      <div className={AppStyles.canvasContainer}>
        <div className={AppStyles.canvasStyle}>
          <div className={AppStyles.headerStyle}>Input</div>
          <div className={AppStyles.canvasWrapper}>
            <canvas
              id="canvasInput"
              width={width}
              height={height}
              ref={this.refHandler.canvasInput}
            />
          </div>
        </div>
        <div className={AppStyles.canvasStyle}>
          <div className={AppStyles.headerStyle}>Output looks like</div>
          <div className={AppStyles.canvasWrapper}>
            <canvas
              id="canvasOutput"
              width={width}
              height={height}
              ref={this.refHandler.canvasOutput}
            />
          </div>
        </div>
      </div>
    );
  };

  private handleDownload = () => {
    const {
      imageDataData,
      valuesPerLine,
      wrapperText,
      nameText,
      useProgmem,
      hasRedColor
    } = this.state;

    const output = getDownloadFile(
      imageDataData,
      valuesPerLine,
      wrapperText,
      nameText,
      hasRedColor,
      useProgmem
    );

    this.downloadFile(`${nameText}.c`, output);
  };

  private downloadFile = (filename: string, text: string) => {
    const element = document.createElement("a");
    element.setAttribute("href", "data:text/plain;charset=utf-8," + encodeURIComponent(text));
    element.setAttribute("download", filename);
    element.style.display = "none";

    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  };

  private handleFileChange = ({ target }: React.ChangeEvent<HTMLInputElement>) => {
    if (target.files && target.files[0]) {
      const reader = new FileReader();
      this.setState({ filename: target.files[0].name });
      reader.onload = this.onFileLoad;
      reader.readAsDataURL(target.files[0]);
    }
  };

  private onFileLoad = ({ target }: FileReaderProgressEvent) => {
    const inputCtx = this.canvasInput.getContext("2d");

    if (inputCtx != null && target != null) {
      const img = new Image();
      img.onload = event => {
        this.setState({
          height: `${img.height}`,
          width: `${img.width}`
        });

        inputCtx.drawImage(img, 0, 0);
        this.convertToOutput();
      };
      img.src = target.result;
    }
  };

  private convertToOutput = () => {
    const { hasRedColor } = this.state;
    const inputCtx = this.canvasInput.getContext("2d");
    const outputCtx = this.canvasOutput.getContext("2d");

    if (inputCtx != null && outputCtx != null) {
      const imageData = inputCtx.getImageData(0, 0, inputCtx.canvas.width, inputCtx.canvas.height);
      const trichromeImageData = convertToTriChrome(imageData, hasRedColor);
      outputCtx.putImageData(trichromeImageData, 0, 0);
      this.setState({ imageDataData: Array.from(trichromeImageData.data) });
    }
  };

  private handleWrapperTextChange = ({ target }: React.ChangeEvent<HTMLTextAreaElement>) =>
    this.setState({ wrapperText: target.value });

  private handleUseProgmemChange = ({ target }: React.ChangeEvent<HTMLInputElement>) =>
    this.setState({ useProgmem: target.checked });

  private handleHasRedColorChange = ({ target }: React.ChangeEvent<HTMLInputElement>) =>
    this.setState({ hasRedColor: target.checked }, this.convertToOutput);

  private handleNameChange = ({ target }: React.ChangeEvent<HTMLInputElement>) =>
    this.setState({ nameText: target.value });
}

export default App;
