import {
  BLACK_PIXEL,
  LUMINOSITY_BLACK,
  LUMINOSITY_RED,
  Pixel,
  PROGMEM_TEXT,
  RED_PIXEL,
  toPixel,
  WHITE_PIXEL
} from "./types";

export const chunkArray = <T>(inputArray: T[], chunkSize: number): T[][] => {
  return Array(Math.ceil(inputArray.length / chunkSize))
    .fill(0)
    .map((_, index) => index * chunkSize)
    .map(begin => inputArray.slice(begin, begin + chunkSize));
};

export const convertToTriChrome = (imageData: ImageData, useRed: boolean) => {
  const { data, width, height } = imageData;

  const newData = [...Array.from(data)];
  for (let i = 0; i < newData.length; i += 4) {
    const pixel: Pixel = toPixel(newData.slice(i, i + 4));

    if (pixelIsBlack(pixel) || (!useRed && pixelIsRed(pixel))) {
      newData.splice(i, 4, ...BLACK_PIXEL);
    } else if (pixelIsRed(pixel)) {
      newData.splice(i, 4, ...RED_PIXEL);
    } else {
      newData.splice(i, 4, ...WHITE_PIXEL);
    }
  }

  return new ImageData(new Uint8ClampedArray(newData), width, height);
};

export const pixelIsRed = (pixel: Pixel) => getLuminosity(pixel) <= LUMINOSITY_RED;
export const pixelIsBlack = (pixel: Pixel) => getLuminosity(pixel) <= LUMINOSITY_BLACK;

export const getLuminosity = (pixel: Pixel) => {
  // red * 0.299 + green * 0.587 + blue * 0.114
  return pixel[0] * 0.299 + pixel[1] * 0.587 + pixel[2] * 0.114;
};

export const isBlack = (pixel: Pixel) => {
  return pixel[0] === 255 && pixel[1] === 255 && pixel[2] === 255;
};

export const isRed = (pixel: Pixel) => {
  return pixel[0] === 255 && pixel[1] === 0 && pixel[2] === 0;
};

export const convertToHex = (byte: boolean[]) => {
  const binary = byte.map(pixel => (pixel ? 1 : 0)).join("");
  const hex = parseInt(binary, 2)
    .toString(16)
    .toUpperCase();

  return "0x" + (hex.length < 2 ? "0" + hex : hex);
};

export const getDownloadFile = (
  imageData: number[],
  valuesPerLine: number,
  wrapperText: string,
  nameText: string,
  hasRedColor: boolean,
  useProgmem: boolean
) => {
  const pixels = chunkArray(imageData, 4);
  const colorFunc = {
    black: isBlack,
    red: isRed
  };

  const colors = ["black"];
  if (hasRedColor) {
    colors.push("red");
  }

  const colorOutput = colors.map(pixelColor => {
    const colorPixels = pixels.map(colorFunc[pixelColor]);
    const bytes = chunkArray(colorPixels, 8).map(convertToHex);

    const lines = chunkArray(bytes, valuesPerLine)
      .map(line => line.join(", "))
      .join(",\n");

    return wrapperText
      .replace("%NAME%", nameText)
      .replace("%CODE%", lines)
      .replace("%COLOR%", pixelColor)
      .replace("=", useProgmem ? "PROGMEM =" : "=");
  });

  return (useProgmem ? PROGMEM_TEXT : "") + colorOutput.join("\n\n");
};
