// Types

export interface IAppState {
  filename: string;
  hasRedColor: boolean;
  height: string;
  imageDataData: number[];
  nameText: string;
  useProgmem: boolean;
  valuesPerLine: number;
  width: string;
  wrapperText: string;
}

export type Pixel = [number, number, number, number];

// Constants

export const LUMINOSITY_RED = 110;

export const LUMINOSITY_BLACK = 50;

export const PROGMEM_TEXT = `#if defined(ESP8266) || defined(ESP32)
#include <pgmspace.h>
#else
#include <avr/pgmspace.h>
#endif

`;

export const DEFAULT_WRAPPER_TEXT = `const unsigned char %NAME%_%COLOR%[] = {
%CODE%
};`;

export const BLACK_PIXEL: Pixel = [0, 0, 0, 255];

export const WHITE_PIXEL: Pixel = [255, 255, 255, 255];

export const RED_PIXEL: Pixel = [255, 0, 0, 255];

export const toPixel = (arr: number[]): Pixel => arr as Pixel;
