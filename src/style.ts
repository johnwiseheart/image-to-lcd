import * as csstips from "csstips";
import { style } from "typestyle";

export namespace AppStyles {
  const primaryColor = "#a5243d";

  const secondaryColor = "#fff";

  export const overlayStyle = style(
    {
      background: "rgba(255, 255, 255, 0.95)",
      bottom: 0,
      left: 0,
      position: "absolute",
      right: 0,
      top: 0
    },
    csstips.centerCenter,
    csstips.flexRoot
  );

  export const checkboxWrapperStyle = style(
    {
      marginRight: 10,
      marginTop: 5
    },
    csstips.inlineBlock
  );

  export const checkboxStyle = style({
    marginRight: 10
  });

  export const labelStyle = style({
    color: "#333",
    fontSize: 10,
    paddingBottom: 5,
    textTransform: "uppercase"
  });

  export const containerStyle = style(csstips.flexRoot, csstips.centerJustified);

  export const appStyle = style(
    {
      minWidth: 600
    },
    csstips.vertical,
    csstips.margin(10)
  );

  export const appHeaderStyle = style({
    color: primaryColor
  });

  export const mainStyle = style(
    {
      position: "relative"
    },
    csstips.vertical,
    csstips.center
  );

  export const fileInputStyle = style({
    display: "none"
  });

  export const buttonInputStyle = style({
    fontSize: 14,
    padding: 5
  });

  export const fileInputLabelStyle = style(
    {
      background: primaryColor,
      borderRadius: 5,
      color: secondaryColor,
      cursor: "pointer",
      marginBottom: 10,
      textAlign: "center"
    },
    csstips.padding(10)
  );

  export const canvasContainer = style(
    {
      marginBottom: 10
    },
    csstips.flexRoot,
    csstips.horizontallySpaced(10)
  );

  export const canvasWrapper = style(
    {
      minHeight: 50
    },
    csstips.flexRoot,
    csstips.centerCenter,
    csstips.padding(10)
  );

  export const canvasStyle = style(
    {
      border: `1px solid ${primaryColor}`,
      borderRadius: 5
    },
    csstips.vertical,
    csstips.flex1
  );

  export const optionsStyle = style({
    border: `1px solid ${primaryColor}`,
    borderRadius: 5,
    marginBottom: 10,
    width: "100%"
  });

  export const headerStyle = style(
    {
      background: primaryColor,
      color: secondaryColor
    },
    csstips.padding(10)
  );

  export const contentStyle = style(csstips.margin(10), csstips.vertical);

  export const textareaStyle = style({
    fontFamily: "monospace",
    marginTop: 5,
    minHeight: 100,
    padding: 5,
    resize: "vertical"
  });

  export const optionsInnerStyle = style(
    csstips.flexRoot,
    csstips.horizontallySpaced(10),
    csstips.padding(10)
  );

  export const optionsItem = style(
    {
      paddingBottom: 5
    },
    csstips.vertical,
    csstips.flex1
  );

  export const downloadStyle = style(
    {
      backgroundColor: primaryColor,
      borderRadius: 5,
      color: secondaryColor,
      cursor: "pointer",
      textAlign: "center",
      width: "100%"
    },
    csstips.padding(10)
  );
}
